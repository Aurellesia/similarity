from django.db import models

class kataDasar(models.Model):
    id_kata_dasar = models.IntegerField(primary_key=True)
    kata_dasar = models.CharField(max_length=20, null=True)
    class Meta:
        db_table = 'dasar'

class stopwords(models.Model):
    id_stopwords = models.IntegerField(primary_key=True)
    stopword = models.CharField(max_length=20, null=True)
    class Meta:
        db_table = 'stopwords'

class termIndex(models.Model):
    id_index = models.IntegerField(primary_key=True)
    nama_doc = models.CharField(max_length=50, null=True)
    term = models.TextField(null=True)
    totalFreq = models.IntegerField(null=True)
    class Meta:
        db_table = 'termindex'

class synonym(models.Model):
    id_sinonim = models.IntegerField(primary_key=True)
    kata = models.CharField(max_length=50, null=True)
    sinonim = models.TextField(null=True)
    class Meta:
        db_table = 'sinonim'

class prepros(models.Model):
    id_index = models.IntegerField(primary_key=True)
    nama_doc = models.CharField(max_length=50, null=True)
    term = models.TextField(null=True)
    totalFreq = models.IntegerField(null=True)
    class Meta:
        db_table = 'prepros'