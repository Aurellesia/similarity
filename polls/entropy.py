from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
import pdftotext
from . import preprocessing
import os, ast
from math import log2
import json
import matplotlib.pylab as plt
from .models import termIndex, synonym


direc = './repo/'
ext = '.pdf'

file_dict = {}
result = {}
hasil = {}
pdf = {}
final = []

txt_files = [i for i in os.listdir(direc) if os.path.splitext(i)[1] == ext]
for x in txt_files:
     with open(os.path.join(direc, x), 'rb') as file_object:
        file_dict[x] = pdftotext.PDF(file_object)
        text = ""
        for page in file_dict[x]:
            text = text + page
            result[x] = text
        pdf = result
        pdfVal = list(pdf.values())
        pdfName = list(pdf.keys())

def docPreprocess(key):
    x = preprocessing.preprocess(key)
    return x

# frekuensi insert ke database
def frequencyDoc(key):
    term = dict()
    for word in key:
        if word in term:
            term[word] += 1
        else:
            term[word] = 1
    return term

def frequencyAll(key):
    allFreq = len(key)
    return allFreq

def main():
    for key in range(len(pdfVal)):
        name = pdfName[key]
        hasil = docPreprocess(pdfVal[key])
        term = frequencyDoc(hasil)
        allFre = frequencyAll(hasil)
        cek = termIndex.objects.filter(nama_doc=name)
        if cek.exists():
            print('{} already in database.'.format(name))
            break
        else:
            db = inDB(name, term, allFre)
    return name

def inDB(names, terms, freq):
    hasil = termIndex(
        nama_doc = names,
        term = terms,
        totalFreq = freq
    )
    hasil.save()
    print('{} successfully saved.'.format(names))

#select db
def takeDB():
    term = []
    cek = termIndex.objects.all().values_list('term', flat=True)
    for n in cek:
        string = ast.literal_eval(n)
        term.append(string)
    return term

def similarity():
    kata = []
    freq = []
    tokenDoc = takeDB()
    query = list(set(queryPreprocess()))

# term yg sama dengan query
    for x in range(len(tokenDoc)):
        kata.append([])
        for y in tokenDoc[x].keys():
            for q in query:
                if q == y:
                    kata[x].append(y)

# ambil frekuensi term yg sama dengan query
    for k in range(len(tokenDoc)):
        freq.append([])
        for m in kata[k]:
            if m in tokenDoc[k]:
                freq[k].append(tokenDoc[k][m])

# ambil total frekuensi dari db
    total = []
    prob = []
    totalFreq = termIndex.objects.all().values_list('totalFreq', flat=True)
    for n in totalFreq:
        total.append(n)

# probability
    for x in range(len(freq)):
        prob.append([])
        for y in freq[x]:
            p = y/total[x]
            prob[x].append(p)
# information
    information = []
    for x in range(len(prob)):
        information.append([])
        for y in prob[x]:
            en = -log2(y)*y
            information[x].append(en)
# entropy
    entropy = []
    for x in range(len(information)):
        entropy.append([])
        en = sum(information[x])
        entropy[x].append(en)
    
    return entropy

# ranking
def rank():
    finalResult = []
    name = totalFreq = termIndex.objects.all().values_list('nama_doc', flat=True)
    ent = similarity()
    dict_entropy = dict(zip(name,ent))
    sort = {sort : v for sort, v in sorted(dict_entropy.items(),reverse=True, key=lambda item: item[1])}

    arr = list(sort)
    for x in range(10):
        finalResult.append(arr[x])

    return sort


# OPTIONAL, jika ingin cek nilai entropy
def checkEntropy():
    entro = []
    doc = rank()
    item = list(doc.items())
    for x in range(10):
        entro.append(item[x])
    return entro

# OPTIONAL, jika ingin melihat grafik
def graph():
    d = similarity()
    lists = sorted(d.items())
    x, y = zip(*lists)
    plt.plot(x, y)
    return plt.show()

# QUERY
def queryPreprocess():
    text = 'ORGANISASI DAN TATA KERJA BALAI PENGELOLAAN HUTAN'
    queryAwal = preprocessing.preprocess(text)

    sinonim = []
    term = []
    kata = synonym.objects.all().values_list('kata', flat=True)
    sin = synonym.objects.all().values_list('sinonim', flat=True)

    for x in sin:
        s = list(x.split(' '))
        sinonim.append(s)
    
    for x in kata:
        term.append(x)
    
    word = dict(zip(term,sinonim))

    a = ' '
    q = []
    for x in queryAwal:
        q.append(x)
        for y in word:
            if x == y:
                for z in word[y]:
                    q.append(z)

    queryExpansion = a.join(q)
    queryAkhir = preprocessing.preprocess(queryExpansion)

    return queryAkhir