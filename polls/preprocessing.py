from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import kataDasar
import re
import csv


def preprocess(text):
    # filtering
    caseFolding = text.lower()
    removeWS = re.sub("^\s+$", "", caseFolding)
    removeRN = re.sub("^\r\n$", " ", removeWS)
    removePT = re.sub("[^a-zA-Z\s]", " ", removeRN)
    removePoints = re.sub("\s[a-z]\s", " ", removePT)
    removeDS = re.sub("\s{2}$", " ", removePoints)
    token = removeDS.split()
    # stopwords
    with open('polls/stopword.txt') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        stop = [i for row in spamreader for i in row]
    result = [x for x in token if x not in stop]
    # stemming
    stem = stemming(result)

    return stem



# cek database

def cek(word):
    hasil = kataDasar.objects.filter(kata_dasar=word)
    if hasil.exists():
        hasil = 1
    else:
        hasil = 0
    return hasil

# menghilangkan sufiks

def inflectionalSuffix(word):
    hsl = word
    if cek(word) != 0:
        hsl = word
    elif word[-3:] == 'lah' or \
            word[-3:] == 'kah' or \
            word[-3:] == 'pun' or \
            word[-3:] == 'nya':
        hsl = word[:-3]
        if cek(hsl) != 0:
            hsl = hsl
    elif word[-2:] == 'ku' or \
            word[-2:] == 'mu':
        hsl = word[:-2]
        if cek(hsl) != 0:
            hsl = hsl
    return hsl

def derivationSuffix(word):
    hsl = word
    if cek(word) != 0:
        hsl = word
    elif word[-1:] == 'i':
        hsl = word[:-1]
        if cek(hsl) != 0:
            hsl = hsl
        else:
            hsl = word
    elif word[-2:] == 'an':
        hsl = word[:-2]
        if cek(hsl) != 0:
            hsl = hsl
        else:
            hsl = word
        if word[-3:] == 'kan':
            hsl = word[:-3]
            if cek(hsl) != 0:
                hsl = hsl
            else:
                hsl = word
    else:
        hsl = word
    return hsl
# menghilangkan prefiks dan sufiks (afiks)

def derivationPrefix(word):
    hsl = word
    if cek(word) != 0:
        hsl = word
    elif word[:2] == 'di' or \
            word[:2] == 'ke' or \
            word[:2] == 'se':
        hsl = word[2:]
        if cek(hsl) != 0:
            hsl = hsl
        else:
            hsl = derivationSuffix(hsl)
            if cek(hsl) != 0:
                hsl = hsl
            else:
                hsl = word
                if hsl[:5] == 'diper' or \
                    hsl[:5] == 'diber' or \
                    hsl[:5] == 'keber' or \
                    hsl[:5] == 'keter':
                    hsl = hsl[5:]
                    if cek(hsl) != 0:
                        hsl = hsl
                    else:
                        hsl = derivationSuffix(hsl)
                        if cek(hsl) != 0:
                            hsl = hsl
                        else:
                            hsl = word
    elif word[:2] == 'be' or \
        word[:2] == 'te':
        hsl = word[2:]
        if cek(hsl) != 0:
            hsl = hsl
        else:
            hsl = derivationSuffix(hsl)
            if cek(hsl) != 0:
                hsl = hsl
            else:
                hsl = word
                if hsl[:3] == 'bel' or \
                    hsl[:3] == 'ber' or \
                    hsl[:3] == 'tel' or \
                    hsl[:3] == 'ter':
                    hsl = hsl[3:]
                    if cek(hsl) != 0:
                        hsl = hsl
                    else:
                        hsl = derivationSuffix(hsl)
                        if cek(hsl) != 0:
                            hsl = hsl
                        else:
                            hsl = word

    elif word[:2] == 'me' or \
        word[:2] == 'pe':
        hsl = word[2:]
        if cek(hsl) != 0:
            hsl = hsl
        else:
            hsl = derivationSuffix(hsl)
            if cek(hsl) != 0:
                hsl = hsl
            else:
                hsl = word
                if hsl[:6] == 'mempel':
                    hsl = hsl[6:]
                    if cek(hsl) != 0:
                        hsl = hsl
                    else:
                        hsl = derivationSuffix(hsl)
                        if cek(hsl) != 0:
                            hsl = hsl
                        else:
                            hsl = word
                if hsl[:6] == 'memper':
                    hsl = hsl[6:]
                    if cek(hsl) != 0:
                        hsl = hsl
                    else:
                        hsl = derivationSuffix(hsl)
                        if cek(hsl) != 0:
                            hsl = hsl
                        else:
                            hsl = word
                if hsl[:6] == 'pembel':
                    hsl = hsl[6:]
                    if cek(hsl) != 0:
                        hsl = hsl
                    else:
                        hsl = derivationSuffix(hsl)
                        if cek(hsl) != 0:
                            hsl = hsl
                        else:
                            hsl = word
                if hsl[:4] == 'meng' or \
                    hsl[:4] == 'peng':
                    hsl = re.sub(hsl[:4], 'k', word)
                    if cek(hsl) != 0:
                        hsl = hsl
                    else:
                        hsl = derivationSuffix(hsl)
                        if cek(hsl) != 0:
                            hsl = hsl
                        else:
                            hsl = word
                            hsl = word[4:]
                            if cek(hsl) != 0:
                                hsl = hsl
                            else:
                                hsl = derivationSuffix(hsl)
                                if cek(hsl) != 0:
                                    hsl = hsl
                                else:
                                    hsl = word
                if hsl[:4] == 'meny' or \
                    hsl[:4] == 'peny':
                    hsl = re.sub(hsl[:4], 's', word)
                    if cek(hsl) != 0:
                        hsl = hsl
                    else:
                        hsl = derivationSuffix(hsl)
                        if cek(hsl) != 0:
                            hsl = hsl
                        else:
                            hsl = word
                            hsl = word[4:]
                            if cek(hsl) != 0:
                                hsl = hsl
                            else:
                                hsl = derivationSuffix(hsl)
                                if cek(hsl) != 0:
                                    hsl = hsl
                                else:
                                    hsl = word
                if hsl[:3] == 'mel' or \
                    hsl[:3] == 'mer' or \
                    hsl[:3] == 'pel' or\
                    hsl[:3] == 'per':
                    hsl = hsl[3:]
                    if cek(hsl) != 0:
                        hsl = hsl
                    else:
                        hsl = derivationSuffix(hsl)
                        if cek(hsl) != 0:
                            hsl = hsl
                        else:
                            hsl = word
                if hsl[:3] == 'men' or \
                    hsl[:3] == 'pen':
                    hsl = re.sub(hsl[:3], 't', word)
                    if cek(hsl) != 0:
                        hsl = hsl
                    else:
                        hsl = derivationSuffix(hsl)
                        if cek(hsl) != 0:
                            hsl = hsl
                        else:
                            hsl = word
                            hsl = word[3:]
                            if cek(hsl) != 0:
                                hsl = hsl
                            else:
                                hsl = derivationSuffix(hsl)
                                if cek(hsl) != 0:
                                    hsl = hsl
                                else:
                                    hsl = word
                if hsl[:3] == 'mem' or \
                    hsl[:3] == 'pem':
                    hsl = re.sub(hsl[:3], 'p', word)
                    if cek(hsl) != 0:
                        hsl = hsl
                    else:
                        hsl = derivationSuffix(hsl)
                        if cek(hsl) != 0:
                            hsl = hsl
                        else:
                            hsl = word
                            hsl = word[3:]
                            if cek(hsl) != 0:
                                hsl = hsl
                            else:
                                hsl = derivationSuffix(hsl)
                                if cek(hsl) != 0:
                                    hsl = hsl
                                else:
                                    hsl = word
    return hsl

# main function stemming

def stemming(teks):
    arr = [derivationSuffix(x) for x in teks]
    arr2 = [inflectionalSuffix(x) for x in arr]
    arr3 = [derivationPrefix(x) for x in arr2]
    return arr3
